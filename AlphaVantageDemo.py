import StockGraphCreator as sgc
import matplotlib.pyplot as plt

def main():
    symbols = ['V','NFLX']
    i = 1
    for s in symbols:
        sgc.create_stock_graph(s)
        i += 1
    plt.show()

if __name__ == "__main__":
    main()



