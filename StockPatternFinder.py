from datetime import datetime


def get_all_lows(stocks):
    lows = []
    for i in range(1, len(stocks)):
        if(stocks[i]['price'] < stocks[i-1]['price'] and stocks[i]['price'] < stocks[i+1]['price']):
            lows.append({'date': stocks[i]['date'], 'price': stocks[i]['price']})

    if(stocks[-1]['price'] < stocks[-2]['price']):
        lows.append({'date': stocks[-1]['date'], 'price': stocks[-1]['price']})
    return lows

def get_all_peaks(stocks):

    peaks = [] # List to store
    # Starting with the second stock in the stocks list...
    for i in range(1, len(stocks) - 1):
        curr_stock = stocks[i]
        prev_stock = stocks[i-1]
        next_stock = stocks[i+1]

        # If the current stock is a high(peak) append it to the list of highs
        if (curr_stock['price'] > prev_stock['price'] and curr_stock['price'] > next_stock['price']):
            peaks.append({'date': curr_stock['date'], 'price': curr_stock['price']})

    # If the final recorded stock is greater than the second to last, record it as a high
    if (stocks[-1]['price'] > stocks[-2]['price']):
        peaks.append({'date': stocks[-1]['date'], 'price': stocks[-1]['price']})

    return peaks

def find_peaks_in_range(peaks, percentRange):
    peaks_in_range = []
    print('num of peaks: {}'.format(len(peaks)))
    for p in range(0, len(peaks)):
        in_ranges = [] # List that will store all peaks in range of current peak

        for r in peaks:
            if(peaks[p] is r): #If initial peak is being compared to itself, skip to the next iteration
                continue
            initial_peak = peaks[p]
            curr_peak = r
            val_difference = abs(initial_peak['price'] - curr_peak['price']) # Difference between curr and initial peak
            allowable_diff = initial_peak['price']*percentRange # Maximum allowed difference between the two peaks
            within_allowable_date_difference = abs((initial_peak['date'] - curr_peak['date']).days) >= 49
            # If the difference between two prices is within the allowable range
            if( val_difference <= allowable_diff):
                if(within_allowable_date_difference): # If the difference in dates is within allowable range
                    in_ranges.append(r) #Append the peak to the in-range list

        if(len(in_ranges) != 0): #If the peak has other peaks in allowable range, add it to the peaks_in_range list
            peaks_in_range.append({'peak': peaks[p], 'in-range': in_ranges})

    return peaks_in_range

def find_cups(stocks, peaks, peaks_in_range):
    cup_list =[]

    #Iterate through the list of stocks that are in range
    for p in peaks_in_range:
        # Iterate through all peaks in range of initial peak
        for r in p['in-range']:

            initial_peak = p['peak']
            last_peak = r
            if (p['peak']['date'] > r['date']):  # If the start date is later than the end date, swap the start and end peaks
                start_copy = initial_peak
                initial_peak = last_peak
                last_peak = start_copy

            start_index = peaks.index(initial_peak) #Find the index to start from in peaks_in_range
            end_index = peaks.index(last_peak) # Find the index to end at in peaks_in_range
            start_price = initial_peak['price'] # initial peak price
            end_price = last_peak['price']

            cup = True # Boolean stores whether or not the list is a cup
            for s in range(start_index + 1, end_index): #iterate through all the peaks from start index to end index
                curr_peak_price = peaks[s]['price']
                #If the stock price is ascending
                if(curr_peak_price >= start_price or curr_peak_price >= end_price): # If the next price is greater than the initial peak price
                    cup = False # It is not a cup, move to the next peak
                    break

            cup = has_prior_uptrend(initial_peak, stocks)

            list = []
            if(cup == True):
                stocks_start_index = stocks.index(initial_peak) # Find the index to start from in stocks list
                stocks_end_index = stocks.index(last_peak) # Find the index to end at in stocks list

                for s in range(stocks_start_index, stocks_end_index):
                    list.append(stocks[s]) # Append all stocks from start to finish to a list

                newlist = sorted(list, key=lambda k: k['price'])
                low_point = newlist[0]
                newlist = [list[0], low_point, list[-1]]



            if(len(list)!= 0):
                list = newlist
                cup_list = add_to_cup_list(newlist, cup_list) # Append list of stocks to the list of cups
                list = []

    return cup_list


def has_prior_uptrend(initial_peak, stock_list):
    over_initial_price = False

    decreasing_prices = []
    stock_index = stock_list.index(initial_peak)
    while (over_initial_price is False):
        old_stock_index = stock_index
        stock_index -= 1
        # If the new stck being analyzed is decreasing from last stock, add it to a list
        if (stock_list[old_stock_index]['price'] > stock_list[stock_index]['price']):
            decreasing_prices.append(stock_list[stock_index])
        elif (stock_list[stock_index]['price'] >= initial_peak['price']):
            break

    decreasing_prices = sorted(decreasing_prices, key=lambda k: k['price'])
    lowest_point = decreasing_prices[0]
    if (lowest_point['price'] <= initial_peak['price'] * .70):
        return True
    else:
        return False


def add_to_cup_list(cup, cup_list):

    if (len(cup_list) == 0):
        cup_list.append(cup)

    duplicate = False
    for c in cup_list:
        if (cup == c):
            duplicate = True

    if(duplicate == False):
        cup_list.append(cup)

    return cup_list

def extract_useless_cups(cups):
    removal_list = []
    for i in range(0, len(cups)):
        j = 0
        while (j < len(cups)):
            if(i == j):
                j+=1
                continue
            first_cup_start_date = cups[i][0]['date'] # initial cup start date
            first_cup_end_date = cups[i][-1]['date'] # initial cup end date
            next_cup_start_date = cups[j][0]['date'] # cup being compared initial cup's start date
            next_cup_end_date = cups[j][-1]['date'] # cup being compared to initial cup's end date
            first_cup_lowpoint_date = cups[i][1]['date'] # lowest point on initial cup
            next_cup_lowpoint_date = cups[j][1]['date'] # cup being compared to initial cup's lowest point
            diff_first_cup_start_and_end = abs(first_cup_start_date - first_cup_end_date)
            diff_next_cup_start_and_end = abs(next_cup_start_date - next_cup_end_date)

            # if both cups have the same lowest point, remove the one that is a shorter time span
            if(first_cup_lowpoint_date == next_cup_lowpoint_date):
                if( diff_first_cup_start_and_end > diff_next_cup_start_and_end ):
                    removal_list = add_cup_to_removal_list(cups[j], removal_list)
                else:
                    removal_list = add_cup_to_removal_list(cups[i], removal_list)
                    break
            # if both cups have the same start or end date, remove the one with the shorter timespan
            elif(first_cup_start_date == next_cup_start_date or first_cup_end_date == next_cup_end_date):
                if(abs(first_cup_start_date - first_cup_end_date) > abs(next_cup_start_date - next_cup_end_date)):
                    removal_list = add_cup_to_removal_list(cups[j], removal_list)
                else:
                    removal_list = add_cup_to_removal_list(cups[i], removal_list)
            # if one cup is within another cup, only keep the longer cup


            j+=1

    cups = [c for c in cups if c not in removal_list]
    return cups

#Ensures that the highest peaks of the cup are chosen to be graphed
def adjust_cup_peaks(stocks, cups, percentRange):
    for c in cups:
        for p in range(stocks.index(c[-1]), len(stocks) - 1):
            value_diff = abs(c[0]['price'] - stocks[p+1]['price']) # Diff between cups initial peak and stock after the last peak
            allowable_diff = stocks[p]['price']*percentRange # Allowable difference between two stock prices
            # If the stock after the cup's second peak is greater than the second peak
            if(stocks[p+1]['price'] > stocks[p]['price']):
                if(value_diff <= allowable_diff):#If it is within the allowable range
                    continue # compare to the next stock
            else:
                c[-1] = stocks[p] # set new second peak
                break

    return cups

def add_cup_to_removal_list(cup, removal_list):

    if (len(removal_list) == 0):
        removal_list.append(cup)

    duplicate = False
    for r in removal_list:
        if (cup == r):
            duplicate = True

    if(duplicate == False):
        removal_list.append(cup)

    return removal_list

def add_prior_uptrend_to_cups(cups, stocks):
    for c in cups:
        initial_peak = c[0]
        stock_index = stocks.index(c[0])
        over_initial_price = False
        decreasing_prices = []
        while (over_initial_price is False):
            old_stock_index = stock_index
            stock_index -= 1
            # If the new stck being analyzed is decreasing from last stock, add it to a list
            if(stocks[old_stock_index]['price'] > stocks[stock_index]['price']):
                decreasing_prices.append(stocks[stock_index])
            elif(stocks[stock_index]['price'] >= initial_peak['price']):
                break

        decreasing_prices = sorted(decreasing_prices, key=lambda k: k['price'])
        lowest_point = decreasing_prices[0]
        if(lowest_point['price'] <= initial_peak['price'] * .85):
            c.insert(0, lowest_point)
        else:
            c.insert(0, initial_peak)

    return cups