# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'StockViewer.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(406, 359)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(90, 130, 211, 31))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.updatePushButton = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.updatePushButton.setObjectName("updatePushButton")
        self.verticalLayout.addWidget(self.updatePushButton)
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(10, 30, 371, 91))
        self.widget.setObjectName("widget")
        self.formLayout = QtWidgets.QFormLayout(self.widget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.stockSymbolLineEdit = QtWidgets.QLineEdit(self.widget)
        self.stockSymbolLineEdit.setMaximumSize(QtCore.QSize(50, 16777215))
        self.stockSymbolLineEdit.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.stockSymbolLineEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.stockSymbolLineEdit.setObjectName("stockSymbolLineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.stockSymbolLineEdit)
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.timeSeriesComboBox = QtWidgets.QComboBox(self.widget)
        self.timeSeriesComboBox.setObjectName("timeSeriesComboBox")
        self.timeSeriesComboBox.addItem("")
        self.timeSeriesComboBox.addItem("")
        self.timeSeriesComboBox.addItem("")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.timeSeriesComboBox)
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.outputSizeComboBox = QtWidgets.QComboBox(self.widget)
        self.outputSizeComboBox.setObjectName("outputSizeComboBox")
        self.outputSizeComboBox.addItem("")
        self.outputSizeComboBox.addItem("")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.outputSizeComboBox)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 406, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.updatePushButton.setText(_translate("MainWindow", "Update"))
        self.label.setText(_translate("MainWindow", "Company Symbol (MSFT, TSLA, NFLX):"))
        self.label_2.setText(_translate("MainWindow", "Time Series:"))
        self.timeSeriesComboBox.setItemText(0, _translate("MainWindow", "Daily"))
        self.timeSeriesComboBox.setItemText(1, _translate("MainWindow", "Weekly"))
        self.timeSeriesComboBox.setItemText(2, _translate("MainWindow", "Monthly"))
        self.label_3.setText(_translate("MainWindow", "Output Size:"))
        self.outputSizeComboBox.setItemText(0, _translate("MainWindow", "Compact"))
        self.outputSizeComboBox.setItemText(1, _translate("MainWindow", "Full"))

