import os
import json
import requests
import datetime


class CompanyStocks:

    def __init__(self, symbol, funct, time_period):
        self.symbol = symbol
        #List of Stocks
        self.stock_list = []
        # List of Stocks that are at a peak between two other stocks
        self.stock_highs = []
        # List of Stocks that are at a valley between two other stocks
        self.stock_lows = []

        # Create a url string based on the information given to the class
        url = self.retrieve_stock_information(symbol, time_period, funct)





    def retrieve_stock_information(self, symbol, time_period, funct):
        websiteName = "https://www.alphavantage.co/query"
        function = funct
        symbol = symbol.upper()
        apiKey = "FVXCZJDSOFW87ODW"
        outputSize = "full"
        time_in_years = time_period

        if (function == "weekly" or function == "TIME_SERIES_MONTHLY_ADJUSTED"):
            url = "{}?function=TIME_SERIES_WEEKLY_ADJUSTED&symbol={}&apikey={}".format(websiteName, symbol, apiKey)
        elif(function == 'monthly'):
            url = "{}?function=TIME_SERIES_MONTHLY_ADJUSTED&symbol={}&apikey={}".format(websiteName, symbol, apiKey)
        elif (function == "daily"):
            url = "{}?function=TIME_SERIES_DAILY_ADJUSTED&symbol={}&outputsize={}&apikey={}".format(websiteName, symbol, outputSize, apiKey)

        self.create_stock_list(url, function)

    def create_stock_list(self, url, funct, time_period):
        # store the list of results in a dict
        json_results = requests.get(url).json()

        if (funct == "daily"):
            stock_dicts = json_results.get('Time Series (Daily)')
        elif (funct == "weekly"):
            stock_dicts = json_results.get('Weekly Adjusted Time Series')
        elif (funct == "monthly"):
            stock_dicts = json_results.get("Monthly Adjusted Time Series")

        # Create a list of dicts that contains information of the stock
        for x in stock_dicts:
            stock_date = datetime.strptime(x, '%Y-%m-%d')
            stock_price = float(stock_dicts[stock_date]['5. adjusted close'])
            stock_volume = stock_dicts[stock_date]['6. volume']

            # If the stock date is within the time period specified, add it to a list of stocks
            if ((datetime.now() - stock_date).days <= time_period * 365):
                self.stock_list.append(Stock(stock_date, stock_price, stock_volume))

class Stock:

    def __init__(self, date, price, volume, percentTrend):