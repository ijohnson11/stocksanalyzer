import os
import json
import requests
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.dates as dates
import numpy as np
from datetime import datetime,timedelta
from requests.auth import HTTPDigestAuth
import scipy.interpolate as interpolate
import StockPatternFinder as spf
import time

def plot_smooth_lines(x,y, linestyle, l):
    sorted_x = sorted(x)
    xnew = dates.date2num(sorted_x)
    f = interpolate.interp1d(xnew, y, kind='quadratic')
    x2 = np.linspace(min(xnew), max(xnew))

    plt.plot(x2, f(x2), linestyle , label= l)


def create_stock_graph(sym):
    print('Creating graph for {}'.format(sym))
    plt.figure(sym)









    # Get a list of stocks in dictionary form




    # Reverse the list to go from oldest date to most recent date
    stocks = list(reversed(stocks))

    # Split the stock dates and prices for graphing
    stock_dates = []
    stock_prices =[]
    for stock in stocks:
        stock_dates.append(stock['date'])
        stock_prices.append(stock['price'])


    #Get difference in highest and lowest values for incrementing y-axis
    highest_close = max(stock_prices)
    lowest_close = min(stock_prices)
    high_low_diff = highest_close - lowest_close


    # Retrieve all instances where the stock price went up and came back down
    peaks = spf.get_all_peaks(stocks)
    peak_prices = []
    peak_dates = []
    for p in peaks:
        peak_dates.append(p['date'])
        peak_prices.append(p['price'])

    # Save all peaks that are within x percent of each other
    peaks_in_range = spf.find_peaks_in_range(peaks, .15)
    # Save a list of cups
    cups = spf.find_cups(stocks, peaks, peaks_in_range)

    # Split the cup prices and dates for graphing, only save the start, end, and low point into the cup list
    c_prices = []
    c_dates = []
    for c in range(0, len(cups)):
        newlist = sorted(cups[c], key=lambda k: k['price'])
        low_point = newlist[0]
        cups[c] = [cups[c][0] ,low_point,cups[c][-1]]

    # Extract any cups that may share the same lowpoint or start and end points
    cups = spf.extract_useless_cups(cups)
    cups = spf.adjust_cup_peaks(stocks, cups, .15)
    #cups = spf.add_prior_uptrend_to_cups(cups,stocks)


    for c in range(0, len(cups)):
        price_list = []
        date_list = []
        for i in cups[c]:
            if(i['date'] not in date_list):
                date_list.append(i['date'])
                price_list.append(i['price'])
        c_prices.append(price_list)
        c_dates.append(date_list)


    plt.xlabel('Date')
    plt.ylabel('Stock Price(US $)')
    plt.title('Stock Chart for {}'.format(symbol))
    plt.xticks(rotation = 45)
    plt.yticks(np.arange(lowest_close,highest_close, step=(high_low_diff/20)))
    plt.plot(stock_dates, stock_prices, 'k', label="Stock Prices")

    for c in cups:
        peak_date = c[-1]['date']
        max_date = c[-1]['date']+ timedelta(days=35)
        xValues = [peak_date, max_date]
        yValues = [c[-1]['price'],c[-1]['price']]
        plt.plot(xValues, yValues, '--r', label='Breakout')
    for c in range(0, len(c_prices)):
        plot_smooth_lines(c_dates[c], c_prices[c], '--b', 'Cups')
    plt.legend(loc='best')

    print('done')
    time.sleep(5)